package nachos.threads;

import java.util.Random;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.threads.PriorityScheduler.PriorityQueue;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
	}

	/**
	 * Allocate a new lottery thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer tickets from
	 *            waiting threads to the owning thread.
	 * @return a new lottery thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryQueue(transferPriority);
	}
	
	protected LotteryThreadState getThreadState(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new LotteryThreadState(thread);
		
		return (LotteryThreadState)thread.schedulingState;
	}
	
	protected class LotteryQueue extends PriorityScheduler.PriorityQueue {
		LotteryQueue(boolean transferPriority) {
			super(transferPriority);
		}
		
		protected LotteryThreadState pickNextThread() {
			if (Wait_thread.isEmpty()) return null;
			int[] Count = new int[Wait_thread.size()];
			int Sum = 0;
			int i = 0;
			for (KThread thread: Wait_thread) {
				Sum = Sum + getThreadState(thread).getEffectivePriority();
				Count[i] = Sum;
				i = i + 1;
			}
			int random = new Random().nextInt(Sum); 
			i = 0;
			for (KThread thread : Wait_thread)
				if (random < Count[i++])
					return getThreadState(thread);
			return null;
		}
	}
	
	protected class LotteryThreadState extends PriorityScheduler.ThreadState {
		public LotteryThreadState(KThread thread) {
			super(thread);
		}
		
		public int getEffectivePriority() {
			if (Effective_priority != Invalid_priority) return Effective_priority;
			Get_donation_priority();
			Get_join_priority();
			return Effective_priority;
		}
		
		private int Get_donation_priority() {
			Effective_priority = priority;
			for (PriorityQueue queue : Transfer_queue) 
				if (queue.transferPriority)
					for (KThread t : queue.Wait_thread) 
						Effective_priority = Effective_priority + getThreadState(t).getEffectivePriority();
			return Effective_priority;
		}
			
		private int Get_join_priority() {
			PriorityQueue queue = (PriorityQueue) thread.Wait_join;
			for (KThread t : queue.Wait_thread)
				Effective_priority = Effective_priority + getThreadState(t).getEffectivePriority();
			return Effective_priority;
		}
	}
	
	private static final int priorityMinimum = 1;
	private static final int priorityMaximum = Integer.MAX_VALUE;
}
