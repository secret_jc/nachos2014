package nachos.threads;

import java.util.LinkedList;

import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;
		
		// Instantiate global variables here
		Adults = adults;
		Children = children;
		Boat = true;
		Finished = false;
		Pilot = false;
		lock = new Lock();
		condition = new Condition(lock);
		LinkedList<KThread> Thread_list = new LinkedList<KThread>();
		
		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.

		for (int i = 0; i < adults; ++i) {
			KThread Thread = new KThread(new Runnable(){
				@Override
				public void run() {
					AdultItinerary();
				}
			});
			Thread.setName("No." + i + "Adult");
			Thread_list.add(Thread);
			Thread.fork();
		}
		
		for (int i = 0; i < children; ++i) {
			KThread Thread = new KThread(new Runnable(){
				@Override
				public void run() {
					ChildItinerary();
				}
			});
			Thread.setName("No." + i + "Child");
			Thread_list.add(Thread);
			Thread.fork();
		}
		for (KThread Thread: Thread_list) {
			Thread.join();
		}
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		lock.acquire();
		while (!(Children <= 1 && Boat))
			condition.sleep();
		Boat = false;
		bg.AdultRowToMolokai();
		Adults = Adults - 1;
		if (Adults == 0 && Children == 0)
			Finished = true;
		condition.wakeAll();
		lock.release();
	}

	static void ChildItinerary() {
		boolean Oahu = true; 
		lock.acquire();
		while (!Finished) {
			if (Boat && Oahu) {
				if (Children >= 2) {
					if (Pilot) { 
						Boat = false;
						bg.ChildRowToMolokai();
						bg.ChildRideToMolokai();
						Children = Children - 2;
						Oahu = false;
						Pilot = false;
						if (Adults == 0 && Children == 0)
							Finished = true;
						condition.wakeAll();
					} 
					else {
						Pilot = true;
						Oahu = false;
					}
				} 
				else {
					if (Children == 1 && Adults == 0) {
						Boat = false;
						bg.ChildRowToMolokai();
						Children = Children - 1;
						Finished = true;
						Oahu = false;
						condition.wakeAll();
					} 
					else 
						condition.sleep();
				}
			} 
			else {
				if (!Boat && !Oahu) {
					bg.ChildRowToOahu();
					Boat = true;
					Oahu = true;
					Children = Children + 1;
					condition.wakeAll();
				} 
				else 
					condition.sleep();
			}
		} 
		lock.release();
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

	static private int Adults, Children;
	static private boolean Finished, Boat, Pilot;
	static private Lock lock;
	static private Condition condition;
}
