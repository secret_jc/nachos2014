package nachos.threads;

import java.util.PriorityQueue;

import nachos.machine.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
	/**
	 * Allocate a new Alarm. Set the machine's timer interrupt handler to this
	 * alarm's callback.
	 * 
	 * <p>
	 * <b>Note</b>: Nachos will not function correctly with more than one alarm.
	 */
	public Alarm() {
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() {
				timerInterrupt();
			}
		});
	}

	/**
	 * The timer interrupt handler. This is called by the machine's timer
	 * periodically (approximately every 500 clock ticks). Causes the current
	 * thread to yield, forcing a context switch if there is another thread that
	 * should be run.
	 */
	public void timerInterrupt() {
		boolean Int_status = Machine.interrupt().disable();

		while (!Wait_queue.isEmpty()) {
			if (Machine.timer().getTime() >= Wait_queue.peek().wakeTime)
				Wait_queue.poll().thread.ready();
			else
				break;
		}
		Machine.interrupt().restore(Int_status);
		
		KThread.yield();
	}

	/**
	 * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
	 * in the timer interrupt handler. The thread must be woken up (placed in
	 * the scheduler ready set) during the first timer interrupt where
	 * 
	 * <p>
	 * <blockquote> (current time) >= (WaitUntil called time)+(x) </blockquote>
	 * 
	 * @param x
	 *            the minimum number of clock ticks to wait.
	 * 
	 * @see nachos.machine.Timer#getTime()
	 */
	public void waitUntil(long x) {
		long wakeTime = Machine.timer().getTime() + x;
		boolean Int_status = Machine.interrupt().disable();
		
		Wait_queue.add(new Wait(KThread.currentThread(), wakeTime));
		KThread.sleep();
		
		Machine.interrupt().restore(Int_status);
	}
	
	private class Wait implements Comparable<Wait> {
		private KThread thread;
		private long wakeTime;
		
		Wait(KThread thread, long Time) {
			this.thread = thread;
			this.wakeTime = Time;
		}
		
		@Override
		public int compareTo(Wait o) {
			if (wakeTime > o.wakeTime) 
				return 1;
			else { 
				if (wakeTime < o.wakeTime) 
					return -1;
				else 
					return 0;
			}
		}
		
	}
	
	private PriorityQueue<Wait> Wait_queue = new PriorityQueue<Wait>();
}
