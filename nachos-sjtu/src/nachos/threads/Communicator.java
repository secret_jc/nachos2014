package nachos.threads;

import java.util.LinkedList;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		lock = new Lock();
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		lock.acquire();
		if (!Listener_list.isEmpty()) {
			Information Listener = Listener_list.removeFirst();
			Listener.word = word;
			Listener.condition.wake();
		} else {
			Information Speaker = new Information();
			Speaker.word = word;
			Speaker_list.add(Speaker);
			Speaker.condition.sleep();
		}
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		lock.acquire();
		int word = 0;
		if (!Speaker_list.isEmpty()) {
			Information speaker = Speaker_list.removeFirst();
			word = speaker.word;
			speaker.condition.wake();
		} else {
			Information listener = new Information();
			Listener_list.add(listener);
			listener.condition.sleep();
			word = listener.word;
		}
		lock.release();
		return word;
	}
	
	private class Information {
		int word;
		Condition condition;
		
		public Information() {
			word = 0;
			condition = new Condition(lock);
		}
	}

	Lock lock;
	
	LinkedList<Information> Speaker_list = new LinkedList<Information>();
	LinkedList<Information> Listener_list = new LinkedList<Information>();
}
