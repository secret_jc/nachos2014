package nachos.vm;

import nachos.machine.TranslationEntry;

public class Page {
	public Page(PID_vpn pid_vpn, TranslationEntry entry) {
		this.pid_vpn = pid_vpn;
		this.entry = entry;
	}

	PID_vpn pid_vpn;
	TranslationEntry entry;
}
