package nachos.vm;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		VMKernel.TLB.Set_empty();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		//super.restoreState();
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		lazy_loader = new Lazy_loader(coff);
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		coff.close();
		VMKernel.TLB.Set_empty();
		for (int i = 0; i < numPages; ++i) {
			PID_vpn pid_vpn = new PID_vpn(PID, i);
			Integer ppn = VMKernel.Inverted_page_table.remove(pid_vpn);
			if (ppn != null) {
				VMKernel.page_dealer.Delete_page(ppn);
				VMKernel.Core_map[ppn].entry.valid = false;
			}
			VMKernel.Get_swap().Remove_swap_page(pid_vpn);
		}
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();
		switch (cause) {
		case Processor.exceptionTLBMiss:
			handleTLBMiss(Processor.pageFromAddress(processor.readRegister(Processor.regBadVAddr)));
			break;
		default:
			super.handleException(cause);
			break;
		}
	}
	
	private void handleTLBMiss(int vpn) {
		TranslationEntry entry = VMKernel.Get_page_entry(new PID_vpn(PID, vpn));	
		if (entry == null) {
			entry = handlePageFault(vpn);
			if (entry == null) handleExit(-1);
		}
		VMKernel.TLB.Put_entry(entry);
	}
	
	private TranslationEntry handlePageFault(int vpn) {
		lock.acquire();
		TranslationEntry entry = VMKernel.page_dealer.Swap_in(new PID_vpn(PID, vpn), lazy_loader);
		lock.release();
		return entry;
	}
	
	protected TranslationEntry Get_entry(int vpn, boolean write) {
		TranslationEntry entry = VMKernel.TLB.Search(vpn, write);
		if (entry == null) {
			handleTLBMiss(vpn);
			entry = VMKernel.TLB.Search(vpn, write);
		}
		return entry;
	}

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	
	private Lazy_loader lazy_loader;
	private static Lock lock = new Lock();
}
