package nachos.vm;

public class PID_vpn {
	int pid, vpn;
	
	public PID_vpn(int pid, int vpn) {
		this.pid = pid;
		this.vpn = vpn;
	}
	
	public boolean equals(Object o) {
		if (o instanceof PID_vpn) {
			PID_vpn pid_vpn = (PID_vpn) o;
			return (pid_vpn.pid == pid && pid_vpn.vpn == vpn);
		}
		else
			return false;
	}
	
	public int hashCode() {
		return pid ^ vpn;
	}
}
