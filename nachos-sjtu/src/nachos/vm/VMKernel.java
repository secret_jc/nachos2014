package nachos.vm;

import java.util.Hashtable;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.TranslationEntry;
import nachos.userprog.UserKernel;

/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
		super();
	}

	/**
	 * Initialize this kernel.
	 */
	public void initialize(String[] args) {
		super.initialize(args);
		TLB = new TLB_dealer();
		Core_map = new Page[Machine.processor().getNumPhysPages()];
		page_dealer = new Page_dealer();
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		super.run();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		Get_swap().close();
		super.terminate();
	}

	public static TranslationEntry Get_page_entry(PID_vpn pid_vpn) {
		Integer ppn = Inverted_page_table.get(pid_vpn);
		if (ppn == null) return null;
		Page res = Core_map[ppn];
		if (res == null || !res.entry.valid) return null;
		return res.entry;
	}
	
	public static Swap_dealer Get_swap() {
		if (swap_dealer == null) swap_dealer = new Swap_dealer();
		return swap_dealer;
	}
	
	private static final char dbgVM = 'v';
	
	protected static Page_dealer page_dealer;
	protected static Swap_dealer swap_dealer;
	protected static TLB_dealer TLB;
	protected static Page[] Core_map;
	protected static Hashtable<PID_vpn, Integer> Inverted_page_table = new Hashtable<PID_vpn, Integer>();
}
