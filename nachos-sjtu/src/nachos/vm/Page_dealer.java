package nachos.vm;

import java.util.HashSet;
import java.util.LinkedList;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.vm.Swap_page;
import nachos.vm.PID_vpn;

public class Page_dealer {
	public Page_dealer() {
		Total_physical_pages = Machine.processor().getNumPhysPages();
		for (int i = 0; i < Total_physical_pages; i++)
			Available_pages.add(i);
	}

	protected int Victim_page() {
		if (!Available_pages.isEmpty())
			return Available_pages.removeFirst();
		Pointer = Pointer % Total_physical_pages;
		while (true) {
			if (Used_pages.contains(Pointer)) {
				if (VMKernel.Core_map[Pointer].entry.used)
					VMKernel.Core_map[Pointer].entry.used = false;
				else {
					Used_pages.remove(new Integer(Pointer));
					int tmp = Pointer;
					Pointer = (Pointer + 1) % Total_physical_pages;
					return tmp;
				}
			}
			Pointer = (Pointer+1) % Total_physical_pages;
		}
	}

	public TranslationEntry Swap_in(PID_vpn pid_vpn, Lazy_loader lazyLoader) {
		VMKernel.TLB.Flush();
		int ppn = Victim_page();
		Swap_out(ppn);
		TranslationEntry entry = lazyLoader.load(pid_vpn, ppn);
		Used_pages.add(ppn);
		VMKernel.Inverted_page_table.put(pid_vpn, ppn);
		VMKernel.Core_map[ppn] = new Page(pid_vpn, entry);
		return entry;
	}

	public void Swap_out(int ppn) {
		Page page = VMKernel.Core_map[ppn];
		if (page != null && page.entry.valid) {
			page.entry.valid = false;
			VMKernel.Inverted_page_table.remove(page.pid_vpn);
			int vpn = page.entry.vpn;
			for (int i = 0; i < Machine.processor().getTLBSize(); i++) {  //Check if it is in the TLB, then invalid it.
				TranslationEntry entry = Machine.processor().readTLBEntry(i);
				if (entry.valid && entry.vpn == vpn) {
					VMKernel.TLB.Invalid(i);
					break;
				}
			}
			if (page.entry.dirty) {
				Swap_page swap_page = VMKernel.Get_swap().New_swap_page(page);
				VMKernel.Get_swap().write(swap_page.Frame, Machine.processor().getMemory(),Processor.makeAddress(ppn, 0));
			}
		}
	}

	protected void Delete_page(int ppn) {
		Used_pages.remove(new Integer(ppn));
		Available_pages.add(ppn);
	}
	
	private int Total_physical_pages;
	private int Pointer;
	private HashSet<Integer> Used_pages = new HashSet<Integer>();
	private LinkedList<Integer> Available_pages = new LinkedList<Integer>();
}
