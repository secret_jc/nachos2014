package nachos.vm;

import nachos.machine.TranslationEntry;

public class Swap_page extends Page {
	public Swap_page(PID_vpn pid_vpn, TranslationEntry entry, int frame) {
		super(pid_vpn, entry);
		this.Frame = frame;
	}
	int Frame;
}
