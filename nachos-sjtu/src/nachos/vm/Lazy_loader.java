package nachos.vm;

import nachos.machine.Coff;
import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.vm.Swap_page;

public class Lazy_loader {
	public Lazy_loader(Coff coff) {
		this.coff = coff;
		int Count_section_page = 0;
		for (int i = 0; i < coff.getNumSections(); i++) {     //Count
			CoffSection section = coff.getSection(i);
			Count_section_page = Count_section_page + section.getLength();
		}
		section_page = new Section_page[Count_section_page];
		for (int i = 0; i < coff.getNumSections(); i++) {  //Allocate code page
			CoffSection section = coff.getSection(i);
			int First_vpn = section.getFirstVPN();
			for (int j = 0; j < section.getLength(); j++) 
				section_page[First_vpn + j] = new Section_page(i, j);
		}
	}

	public TranslationEntry load(PID_vpn pid_vpn, int ppn) {
		TranslationEntry entry;
		Swap_page swap_page = VMKernel.Get_swap().Get_swap_page(pid_vpn);   //Get from swap file
		if (swap_page != null) {   // in swap file
			entry = swap_page.entry;
			entry.ppn = ppn;
			entry.valid = true;
			entry.used = false;
			entry.dirty = false;
			VMKernel.Get_swap().read(swap_page.Frame, Machine.processor().getMemory(), Processor.makeAddress(ppn, 0));
		}
		else {  //not in swap file
			if (pid_vpn.vpn >= 0 && pid_vpn.vpn < section_page.length) {  // in code
				CoffSection section = coff.getSection(section_page[pid_vpn.vpn].section);
				entry = new TranslationEntry(pid_vpn.vpn, ppn, true, section.isReadOnly(), false, false);
				section.loadPage(section_page[pid_vpn.vpn].offset, ppn);
			}
			else {  // in stack
				Clean(ppn);
				entry = new TranslationEntry(pid_vpn.vpn, ppn, true, false, false, false);
			}
		}
		return entry;
	}

	private void Clean(int ppn) {
		byte[] mem = Machine.processor().getMemory();
		int Begin = Processor.makeAddress(ppn, 0);
		int End = Begin + Processor.pageSize;
		for (int i = Begin; i < End; i++)
			mem[i] = 0;
	}

	class Section_page {
		public Section_page(int section, int offset) {
			this.section = section;
			this.offset = offset;
		}
		public int section;
		public int offset;
	}

	private Coff coff;
	private Section_page[] section_page;
}
