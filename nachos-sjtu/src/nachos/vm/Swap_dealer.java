package nachos.vm;

import java.util.Hashtable;
import java.util.LinkedList;

import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.ThreadedKernel;

public class Swap_dealer {
	public Swap_dealer() {
		Total_frame = 0;
		Swap_file = ThreadedKernel.fileSystem.open(Swap_name, true);
	}
	
	public int New_frame() {
		if (Free_frames.isEmpty()) return Total_frame++;
		Integer index = Free_frames.removeFirst();
		return index;
	}
	
	public Swap_page New_swap_page(Page page) {
		Swap_page swap_page = Swap_table.get(page.pid_vpn);
		if (swap_page == null) {
			swap_page = new Swap_page(page.pid_vpn, page.entry, New_frame());
			Swap_table.put(page.pid_vpn, swap_page);
		}
		return swap_page;
	}
	
	public boolean Remove_swap_page(PID_vpn pid_vpn) {
		Swap_page swap_page = Get_swap_page(pid_vpn);
		if (swap_page == null) return false;
		Free_frames.add(swap_page.Frame);
		return true;
	}

	public Swap_page Get_swap_page(PID_vpn pid_vpn) {
		return Swap_table.get(pid_vpn);
	}
	
	public boolean write(int frame, byte[] res, int offset) {
		int amount = Swap_file.write(frame * Processor.pageSize, res, offset, Processor.pageSize);
		return (amount == Processor.pageSize);
	}
	
	public boolean read(int fram, byte[] res, int offset) {
		int amount = Swap_file.read(fram*Processor.pageSize, res, offset, Processor.pageSize);
		return (amount == Processor.pageSize);
	}
	
	public void close() {
		Swap_file.close();
		ThreadedKernel.fileSystem.remove(Swap_name);
	}
	
	public OpenFile Get_swap_file() {
		return Swap_file;
	}
	
	public static final String Swap_name = "SWAP";
	
	private int Total_frame;
	private OpenFile Swap_file;
	private Hashtable<PID_vpn, Swap_page> Swap_table = new Hashtable<PID_vpn, Swap_page>();
	private LinkedList<Integer> Free_frames = new LinkedList<Integer>();
}
