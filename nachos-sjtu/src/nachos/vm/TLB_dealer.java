package nachos.vm;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.TranslationEntry;

public class TLB_dealer {
	
	public void Put_entry(TranslationEntry entry) {
		int pos = Lib.random(Machine.processor().getTLBSize());
		for (int i = 0; i < Machine.processor().getTLBSize(); i++)
			if (!Machine.processor().readTLBEntry(i).valid) {
				pos = i;
				break;
			}
		TLB_pagetable(pos);
		Machine.processor().writeTLBEntry(pos, entry);
	}
	
	public void TLB_pagetable(int i) {      //Take from TLB to pagetable
		TranslationEntry entry = Machine.processor().readTLBEntry(i);
		if (entry.valid)
			VMKernel.Core_map[entry.ppn].entry = entry;
	}

	public void Set_empty() {
		Flush();
		Invalid_all();
	}
	
	public void Flush() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++)
			TLB_pagetable(i);
	}
	
	public void Invalid_all() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++)
			Invalid(i);
	}
	
	public void Invalid(int i) {
		TranslationEntry entry = Machine.processor().readTLBEntry(i);
		if (!entry.valid)
			return;
		entry.valid = false;
		Machine.processor().writeTLBEntry(i, entry);
	}

	public TranslationEntry Search(int vpn, boolean write) {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
			TranslationEntry entry = Machine.processor().readTLBEntry(i);
			if (entry.valid && entry.vpn == vpn) {
				if (entry.readOnly && write)
					return null;
				entry.dirty = entry.dirty || write;
				entry.used = true;
				Machine.processor().writeTLBEntry(i, entry);
				return entry;
			}
		}
		return null;
	}
}
