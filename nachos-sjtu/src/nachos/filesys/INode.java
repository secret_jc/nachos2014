package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Machine;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
  /** represent a system file (free list) */
  public static int TYPE_SYSTEM = 0;
  
  /** represent a folder */
  public static int TYPE_FOLDER = 1;
  
  /** represent a normal file */
  public static int TYPE_FILE = 2;
  
  /** represent a normal file that is marked as delete */
  public static int TYPE_FILE_DEL = 3;
  
  /** represent a symbolic link file */
  public static int TYPE_SYMLINK = 4;
  
  /** represent a folder that are not valid */
  public static int TYPE_FOLDER_DEL = 5;
  
  /** the reserve size (in byte) in the first sector */
  private static final int FIRST_SEC_RESERVE = 16;
  
  /** size of the file in bytes */
  int file_size;
  
  /** the type of the file */
  int file_type;
  
  /** the number of programs that have access on the file */
  int use_count;
  
  /** the number of links on the file */
  int link_count;
  
  /** maintain all the sector numbers this file used in order */
  private LinkedList<Integer> sec_addr;
  
  /** the first address */
  private int addr;
  
  /** the extended address */
  private LinkedList<Integer> addr_ext;
  
  File_lock access = new File_lock();
  
  public INode (int addr)
  {
    file_size = 0;
    file_type = TYPE_FILE;
    use_count = 0;
    link_count = 0;
    sec_addr = new LinkedList<Integer>();
    this.addr = addr;
    addr_ext = new LinkedList<Integer>();
  }
  
  /** get the sector number of a position in the file  */
  public Integer getSector (int pos)
  {
    if (pos >= sec_addr.size()) return null;
    return sec_addr.get(pos);
  }
  
  /** change the file size and adjust the content in the inode accordingly */
  public void setFileSize (int size)
  {
    if (file_size == size) return;
    file_size = size;
    int Total_sector = file_size / Disk.SectorSize;
    if (file_size % Disk.SectorSize > 0) 
    	Total_sector = Total_sector + 1;
    int Size_sector = sec_addr.size();
    if (Total_sector == Size_sector) return;
    if (Total_sector > Size_sector) {
    	for (int i = Size_sector + 1; i <= Total_sector; i++)
    		sec_addr.add(FilesysKernel.realFileSystem.getFreeList().allocate());
    }
    if (Total_sector < Size_sector) {
		for (int i = Total_sector + 1; i <= Size_sector; i++)
			FilesysKernel.realFileSystem.getFreeList().deallocate(sec_addr.remove(i));
    }
  }
  
  /** free the disk space occupied by the file (including inode) */
  public void free ()
  {
    for (Integer sector: sec_addr)
    	FilesysKernel.realFileSystem.getFreeList().deallocate(sector);
	for (Integer ex_addr : addr_ext)
		FilesysKernel.realFileSystem.getFreeList().deallocate(ex_addr);
    file_size = 0;
    sec_addr.clear();
    addr_ext.clear();
	FilesysKernel.realFileSystem.getFreeList().deallocate(addr);
	RealFileSystem.Remove_Inode(addr);
  }
  
  /** load inode content from the disk */
  public void load ()
  {
    byte[] data = new byte[Disk.SectorSize];
	Machine.synchDisk().readSector(addr, data, 0);	
	file_size = Disk.intInt(data, 0);
	file_type = Disk.intInt(data, 4);
	link_count = Disk.intInt(data, 8);
	int Total_ex_addr = Disk.intInt(data, 12);
	int Total_sector = Disk.intInt(data, 16);
	int pointer = 20;
	int sector = 0;
	for (int i = 0; i < Total_ex_addr; i++) {
		if (pointer == Disk.SectorSize) {
			pointer = 0;
			Machine.synchDisk().readSector(addr_ext.get(sector++), data, 0);
		}
		addr_ext.add(Disk.intInt(data, pointer));
		pointer = pointer + 4;
	}
	for (int i = 0; i < Total_sector; i++) {
		if (pointer == Disk.SectorSize) {
			pointer = 0;
			Machine.synchDisk().readSector(addr_ext.get(sector++), data, 0);
		}
		sec_addr.add(Disk.intInt(data, pointer));
		pointer = pointer + 4;
	}
  }
  
  /** save inode content to the disk */
  public void save ()
  {
	if (addr == -1) return;  
	int address_need = 20 + (addr_ext.size() + sec_addr.size()) * 4;
	while (address_need > (addr_ext.size() + 1) * Disk.SectorSize) {
		address_need = address_need + 4;
		addr_ext.add(FilesysKernel.realFileSystem.getFreeList().allocate());
	}
	byte[] buffer = new byte[Disk.SectorSize];
	Disk.extInt(file_size, buffer, 0);
	Disk.extInt(file_type, buffer, 4);
	Disk.extInt(link_count, buffer, 8);
	Disk.extInt(addr_ext.size(), buffer, 12);
	Disk.extInt(sec_addr.size(), buffer, 16);
	int pointer = 20;
	int address = addr;
	int sector = 0;
	for (int i = 0; i < addr_ext.size(); i++) {
		if (pointer == Disk.SectorSize) {
			pointer = 0;
			Machine.synchDisk().writeSector(address, buffer, 0);
			address = addr_ext.get(sector++);
		}
		Disk.extInt(addr_ext.get(i), buffer, pointer);
		pointer = pointer + 4;
	}
	for (int i = 0; i < sec_addr.size(); i++) {
		if (pointer == Disk.SectorSize) {
			pointer = 0;
			Machine.synchDisk().writeSector(address, buffer, 0);
			address = addr_ext.get(sector++);
		}
		Disk.extInt(sec_addr.get(i), buffer, pointer);
		pointer = pointer + 4;
	}
	Machine.synchDisk().writeSector(address, buffer, 0);
  }

  public void Decrease_use_count() {
	use_count = use_count - 1;
	Free_attempt();
  }

  public void Free_attempt() {
	if (use_count == 0 && link_count == 0)
		free();
  }
  
  public int sector_count() {
	return sec_addr.size();
  }
}
