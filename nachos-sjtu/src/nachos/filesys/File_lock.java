package nachos.filesys;

import java.util.ArrayList;

import nachos.machine.Machine;
import nachos.threads.*;

public class File_lock {
	public File_lock() {
	}
	
	public void Get_read_access() {
		boolean intStatus = Machine.interrupt().disable();
		KThread thread = KThread.currentThread();
		if (Write_thread != null || Write_wait_number > 0) {
			Read_waiting.waitForAccess(thread);
			KThread.sleep();
		} else {
			Read_waiting.acquire(thread);
			Read_thread.add(thread);
		}
		Machine.interrupt().restore(intStatus);
	}
	
	public void Free_read_access() {
		boolean intStatus = Machine.interrupt().disable();
		Read_thread.remove(KThread.currentThread());
		if (Read_thread.isEmpty()) {
			if ((Write_thread = Write_waiting.nextThread()) != null) {
				--Write_wait_number;
				Write_thread.ready();
			}
		}
		Machine.interrupt().restore(intStatus);
	}
	
	public void Get_write_access() {
		boolean intStatus = Machine.interrupt().disable();
		KThread thread = KThread.currentThread();
		
		if (Write_thread != null || Read_thread.size() > 0) {
			Write_waiting.waitForAccess(thread);
			++Write_wait_number;
			KThread.sleep();
		} else {
			Write_waiting.acquire(thread);
			Write_thread = thread;
		}
		Machine.interrupt().restore(intStatus);
	}
	
	public void Free_write_access() {
		boolean intStatus = Machine.interrupt().disable();
		if ((Write_thread = Write_waiting.nextThread()) != null) {
			--Write_wait_number;
			Write_thread.ready();
		} else {
			KThread reader = null;
			while ((reader = Read_waiting.nextThread()) != null) {
				reader.ready();
				Read_thread.add(reader);
			}
		}
		Machine.interrupt().restore(intStatus);
	}
	
	public boolean Cur_thread_read() {
		return Read_thread.contains(KThread.currentThread());
	}
	
	public boolean Cur_thread_write() {
		return Write_thread == KThread.currentThread();
	}

	int Write_wait_number = 0;
	private ArrayList<KThread> Read_thread = new ArrayList<KThread>();
	private KThread Write_thread = null;
	private ThreadQueue Read_waiting = ThreadedKernel.scheduler.newThreadQueue(true);
	private ThreadQueue Write_waiting = ThreadedKernel.scheduler.newThreadQueue(true);
}
