package nachos.filesys;

import java.util.Hashtable;

import nachos.machine.Disk;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
  /** the static address for root folder */
  public static int STATIC_ADDR = 1;
  
  private int size;
  
  /** mapping from filename to folder entry */
  private Hashtable<String, FolderEntry> entry;
  
  public Folder (INode inode)
  {
    super(inode);
    size = 4;
    inode.file_type = INode.TYPE_FOLDER;
    entry = new Hashtable<String, FolderEntry>();
  }
  
  /** open a file in the folder and return its address */
  public int open (String filename)
  {
	FolderEntry folderEntry = entry.get(filename);
	if (folderEntry == null) 
		return -1;
	return folderEntry.addr;
  }
  
  /** create a new file in the folder and return its address */
  public int create (String filename)
  {
	int addr = FilesysKernel.realFileSystem.getFreeList().allocate();
	INode inode = RealFileSystem.Get_Inode(addr);
	inode.file_type = INode.TYPE_FILE;
	addEntry(filename, addr);
	return addr;
  }
  
  /** add an entry with specific filename and address to the folder */
  public void addEntry (String filename, int addr)
  {
	INode inode = RealFileSystem.Get_Inode(addr);
	inode.link_count++;
	entry.put(filename, new FolderEntry(filename, addr));
	save();
  }
  
  /** remove an entry from the folder */
  public void removeEntry (String filename)
  {
	entry.remove(filename);
  }
  
  /** save the content of the folder to the disk */
  public void save ()
  {
	Get_write_access();	
	size = 4;
	for (FolderEntry folderEntry : entry.values())
		size += 4 + (folderEntry.name.length() + 1);
	byte[] buf = new byte[size];
	Disk.extInt(entry.size(), buf, 0);
	int pointer = 4;
	for (FolderEntry Sub_file : entry.values()) {
		
		String file_name = Sub_file.name;
		System.arraycopy(file_name.getBytes(), 0, buf, pointer, file_name.length());
		buf[pointer + file_name.length()] = 0;
		
		pointer += Sub_file.name.length() + 1;
		Disk.extInt(Sub_file.addr, buf, pointer);
		pointer += 4;
	}
	write(0, buf, 0, buf.length);
	inode.setFileSize(size);
	inode.save();
	Free_write_access();
  }
  
  /** load the content of the folder from the disk */
  public void load ()
  {
	Get_read_access();
	byte[] buf = new byte[length()];
	read(0, buf, 0, buf.length); 
	int pointer = 4;
	int Sub_file_num = Disk.intInt(buf, 0);
	for (int i = 0; i < Sub_file_num; ++i) {
		String file_name = null;
		for (int length = pointer; length < Math.min(pointer + RealFileSystem.Max_string_length + 1, buf.length); ++length) 
			if (buf[length] == 0) {
				file_name = new String(buf, pointer, length - pointer);
				break;
			}
		
		pointer += file_name.length() + 1;
		int addr = Disk.intInt(buf, pointer);
		pointer += 4;
		entry.put(file_name, new FolderEntry(file_name, addr));
	}
	Free_read_access();
  }
  
  public boolean contains(String filename) {
	return entry.containsKey(filename);
  }
  
  public int Get_folder_addr(String filename) {
	FolderEntry folder = entry.get(filename);
	if (folder == null)
		return -1;
	return folder.addr;
  }

  public boolean empty() {
	return entry.isEmpty();
  }
  
  public String[] Folder_content() {
	String[] res = new String[entry.size()];
	int i = 0;
	for (String st : entry.keySet())
		res[i++] = st;
	return res;
  }
}
