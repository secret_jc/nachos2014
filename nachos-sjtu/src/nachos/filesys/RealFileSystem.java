package nachos.filesys;

import java.util.Hashtable;
import java.util.LinkedList;

import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.vm.VMKernel;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
  /** the free list */
  private static FreeList free_list;
  
  /** the root folder */
  private Folder root_folder;
  
  /** the current folder */
 // private Folder cur_folder;
  private LinkedList<Folder> cur_folder = new LinkedList<Folder>();
  
  /** the string representation of the current folder */
  private LinkedList<String> cur_path = new LinkedList<String>();
  
  private static String path_seperator = "/";
  private static String Current_folder = ".";
  private static String Last_folder = "..";
	
  public static final int Max_string_length = 256;
  private static final int Max_path = 256;
  private static Hashtable<Integer, INode> Map_inode = new Hashtable<Integer, INode>();
  
  /**
   * initialize the file system
   * 
   * @param format
   *          whether to format the file system
   */
  public void init (boolean format)
  {
    if (format)
    {
	  INode inode_free_list = new INode(FreeList.STATIC_ADDR);
	  free_list = new FreeList(inode_free_list);
	  free_list.init();
	  INode inode_root_folder = new INode(Folder.STATIC_ADDR);
	  root_folder = new Folder(inode_root_folder);
	  root_folder.save();
	  inode_root_folder.save();
	  cur_folder.add(root_folder);
	  Map_inode.put(FreeList.STATIC_ADDR, inode_free_list);
	  Map_inode.put(Folder.STATIC_ADDR, inode_root_folder);
	  
	  importStub();
    }
    else
    {
      INode inode_free_list = new INode(FreeList.STATIC_ADDR);
      inode_free_list.load();
      free_list = new FreeList(inode_free_list);
      free_list.load();
      INode inode_root_folder = new INode(Folder.STATIC_ADDR);
      inode_root_folder.load();
      root_folder = new Folder(inode_root_folder);
      root_folder.load();
	  Map_inode.put(FreeList.STATIC_ADDR, inode_free_list);
      Map_inode.put(Folder.STATIC_ADDR, inode_root_folder);
	  cur_folder.add(root_folder);
    }
	free_list.init_used_list();
  }
  
  public void finish ()
  {
	VMKernel.Get_swap().close();
	free_list.save();
	for (INode inode : Map_inode.values())
		inode.save();
  }
  
  /** import from stub filesystem */
  private void importStub ()
  {
    FileSystem stubFS = Machine.stubFileSystem();
    FileSystem realFS = FilesysKernel.realFileSystem;
    String[] file_list = Machine.stubFileList();
    for (int i = 0; i < file_list.length; ++i)
    {
      if (!file_list[i].endsWith(".coff"))
        continue;
      OpenFile src = stubFS.open(file_list[i], false);
      if (src == null)
      {
        continue;
      }
      OpenFile dst = realFS.open(file_list[i], true);
      int size = src.length();
      byte[] buffer = new byte[size];
      src.read(0, buffer, 0, size);
      dst.write(0, buffer, 0, size);
      src.close();
      dst.close();
    }
  }
  
  /** get the only free list of the file system */
  public FreeList getFreeList ()
  {
    return free_list;
  }
  
  /** get the only root folder of the file system */
  public Folder getRootFolder ()
  {
    return root_folder;
  }
  
  public OpenFile open(String name, boolean create)
  {
	Path_content res = String_to_path(name);
	if (res.found) {
		if (res.addr >= 0) {
			INode inode = Get_Inode(res.addr);
			if (inode.file_type == INode.TYPE_FILE) {
				inode.use_count++;
				return new File(inode);
			} else {
				if (inode.file_type == INode.TYPE_SYMLINK)
					return Load_symlink(inode);
			}
		} else {
			if (create) {
				INode inode = Get_Inode(res.cur.create(res.filename));
				inode.use_count++;
				return new File(inode);
			}
		}
	}
	return null;
  }
  
  public boolean remove (String name)
  {
	Path_content res = String_to_path(name);
	if (res.found && res.addr != -1) {
		INode inode = Get_Inode(res.addr);
		if (inode.file_type == INode.TYPE_FILE || inode.file_type == INode.TYPE_SYMLINK) {
			res.cur.removeEntry(res.filename);
			res.cur.save();
			inode.link_count--;
			if (inode.link_count == 0)
				inode.file_type = INode.TYPE_FILE_DEL;
			inode.Free_attempt();
			return true;
		}
	}
	return false;
  }
  
  public boolean createFolder (String name)
  {
	Path_content res = String_to_path(name);
	if (!res.found || res.addr != -1)
		return false;
	int addr = free_list.allocate();
	INode inode = Get_Inode(addr);
	Folder newFolder = new Folder(inode);
	newFolder.save();
	res.cur.addEntry(res.filename, addr);
	return true;
  }
  
  public boolean removeFolder (String name)
  {
	Path_content res = String_to_path(name);
	if (!res.found || res.addr == -1)
		return false;
	if (res.addr == Folder.STATIC_ADDR)
		return false;
	INode inode = Get_Inode(res.addr);
	if (inode.file_type == INode.TYPE_FOLDER) {
		Folder folder = new Folder(inode);
		folder.load();
		if (folder.empty()) {
			res.cur.removeEntry(res.filename);
			res.cur.save();
			inode.link_count--;
			if (inode == cur_folder.getLast().inode) {
				cur_folder.removeLast();
				cur_path.removeLast();
			}
			inode.Free_attempt();
			return true;
		} else 
			return false;
	}
	return false;
  }
  
  public boolean changeCurFolder(String name)
  {
	Path_content res = String_to_path(name);
	if (!res.found || res.addr == -1)
		return false;
	INode inode = Get_Inode(res.addr);
	if (inode.file_type == INode.TYPE_FOLDER) {
		Folder folder = new Folder(inode);
		folder.load();
		cur_folder = res.folder;
		cur_folder.add(folder);
		cur_path = res.path;
		if (res.filename != null)
			cur_path.add(res.filename);
		return true;
	}
	return false;
  }
  
  public String[] readDir (String name)
  {
	Path_content res = String_to_path(name);
	if (!res.found || res.addr == -1)
		return null;
	INode inode = Get_Inode(res.addr);
	if (inode.file_type == INode.TYPE_FOLDER) {
		Folder folder = new Folder(inode);
		folder.load();
		return folder.Folder_content();
	}
	return null;
  }
  
  public FileStat getStat (String name)
  {
	Path_content res = String_to_path(name);
	if (!res.found || res.addr == -1)
		return null;
	INode inode = Get_Inode(res.addr);
	if (inode.file_type == INode.TYPE_FILE_DEL || inode.file_type == INode.TYPE_FOLDER_DEL)
		return null;
	FileStat stat = new FileStat();
	stat.name = res.filename;
	stat.size = inode.file_size;
	stat.sectors = inode.sector_count();
	stat.inode = res.addr;
	stat.links = inode.link_count;
	stat.type = FileStat.Translate.get(inode.file_type);
	return stat;
  }
  
  public boolean createLink (String src, String dst)
  {
	Path_content resSrc = String_to_path(src);
	if (!resSrc.found || resSrc.addr == -1)
		return false;
	Path_content resDst = String_to_path(dst);
	if (!resDst.found || resDst.addr != -1)
		return false;
	resDst.cur.addEntry(resDst.filename, resSrc.addr);
	return true;
  }
  
  public boolean createSymlink (String src, String dst)
  {
	Path_content resDst = String_to_path(dst);
	if (!resDst.found || resDst.addr != -1)
		return false;
	int addr = free_list.allocate();
	INode inode = Get_Inode(addr);
	File file = new File(inode);
	inode.file_type = INode.TYPE_SYMLINK;
	file.Write_link(0, src);
	resDst.cur.addEntry(resDst.filename, addr);
	return true;
  }
  
  public int getFreeSize()
  {
	return free_list.size();
  }
  
  public int getSwapFileSectors()
  {
	//INode inode = ((File) VMKernel.Get_swap().Get_swap_file()).inode;
	return 0;
  }
  
  public static INode Get_Inode(int addr) {
	INode inode = Map_inode.get(addr);
	if (inode == null) {
	    inode = new INode(addr);
		if (free_list.Check_used(addr))
			inode.load();
		Map_inode.put(addr, inode);
	}
	return inode;
  }
  
  public static void Remove_Inode(int addr) {
	Map_inode.remove(addr);
  }
  
  private OpenFile Load_symlink(INode inode) {
	File file = new File(inode);
	String name = file.Read_link(0, Max_path);
	return Open_symlink(name);
  }
	
  public OpenFile Open_symlink(String name) {
	Path_content res = String_to_path(name);
	if (res.found)
		if (res.addr >= 0) {
			INode inode = Get_Inode(res.addr);
			if (inode.file_type == INode.TYPE_FILE) {
				inode.use_count++;
				return new File(inode);
			}
		}
	return null;
  }
  
  public String Current_path_string() {
	LinkedList<String> path = cur_path;
	StringBuffer res = new StringBuffer();
	for (String st : path)
		res.append("/" + st);
	if (res.length() == 0)
		res.append("/");
	return res.toString();
  }
  
  class Path_content {
	int addr = -1;
	Folder cur = null;
	String filename = null;
	LinkedList<String> path = null;
	LinkedList<Folder> folder = null;
	boolean found = false;
  }
	
  private Path_content String_to_path(String name) {
	Path_content res = new Path_content();
	LinkedList<String> path = new LinkedList<String>();
	LinkedList<Folder> folder = new LinkedList<Folder>();
	while (name.length() > 1 && name.endsWith(path_seperator))
		name = name.substring(0, name.length() - 1);
	if (name.startsWith(path_seperator)) 
		folder.add(root_folder);
	else {
		path.addAll(cur_path);
		folder.addAll(cur_folder);
	}
	if (name.equals(path_seperator)) {
		res.found = true;
		res.cur = null;
		res.path = path;
		res.folder = folder;
		res.folder.removeLast();
		res.addr = Folder.STATIC_ADDR;
		return res;
	}	
	String[] Folder_list = name.split(path_seperator);
	String Final_file = Folder_list[Folder_list.length - 1];
	for (String Name : Folder_list) {
		if (Name.isEmpty() || Name.equals(Current_folder) && Name != Final_file)
			continue;
		if (Name.equals(Last_folder)) {
			folder.removeLast();
			if (path.isEmpty()) return res;
			path.removeLast();
			if (folder.isEmpty()) return res;
			if (Name != Final_file) continue;
		}		
		Folder cur = folder.getLast();
		cur.load();
		if (Name == Final_file) {
			res.found = true;
			res.cur = cur;
			res.filename = Final_file;
			res.path = path;
			res.folder = folder;
			if (Name.equals(Current_folder) || Name.equals(Last_folder)) {
				if (res.folder.size() > 1) {
					res.cur = folder.get(folder.size() - 2);
					res.filename = path.getLast();
					res.addr = res.cur.Get_folder_addr(res.filename);
					return res;
				} else {
					if (res.folder.size() == 1) {
						res.addr = Folder.STATIC_ADDR;
						res.cur = null;
						res.filename = null;
						res.folder.clear();
						res.path.clear();
						return res;
					} else {
						res.found = false;
						return res;
					}
				}
			} else {
				if (cur.contains(Name)) {
					res.addr = cur.Get_folder_addr(Final_file);
					return res;
				} else 
					return res;
			}
		}
		if (cur.contains(Name)) {
			int inodeAddr = cur.Get_folder_addr(Name);
			INode inode = Get_Inode(inodeAddr);
			if (inode.file_type == INode.TYPE_FOLDER) {
				path.add(Name);
				Folder next = new Folder(inode);
				folder.add(next);
			} else 
				return res;
		} else 
			return res;
	}
	return res;
  }
}
