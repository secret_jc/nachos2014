package nachos.filesys;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
  protected static final int SYSCALL_MKDIR = 14;
  protected static final int SYSCALL_RMDIR = 15;
  protected static final int SYSCALL_CHDIR = 16;
  protected static final int SYSCALL_GETCWD = 17;
  protected static final int SYSCALL_READDIR = 18;
  protected static final int SYSCALL_STAT = 19;
  protected static final int SYSCALL_LINK = 20;
  protected static final int SYSCALL_SYMLINK = 21;
 
  public static final int Max_string_length = RealFileSystem.Max_string_length;
  
  public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
  {
    switch (syscall)
    {
      case SYSCALL_MKDIR:
      {
    	  if (FilesysKernel.realFileSystem.createFolder(readVirtualMemoryString(a0, Max_string_length)))
    	  	return 0;
    	  else return -1;
      } 
      case SYSCALL_RMDIR:
      {
    	  if (FilesysKernel.realFileSystem.removeFolder(readVirtualMemoryString(a0, Max_string_length)))
      	  	return 0;
      	  else return -1;
      } 
      case SYSCALL_CHDIR:
      {
    	  if (FilesysKernel.realFileSystem.changeCurFolder(readVirtualMemoryString(a0, Max_string_length)))
      	  	return 0;
      	  else return -1;
      }
      case SYSCALL_GETCWD:
    	  return handleGetcwd(a0, a1);
	  case SYSCALL_READDIR:
		  return handleReaddir(a0, a1, a2, a3);
	  case SYSCALL_STAT:
		  return handleStat(a0, a1);
	  case SYSCALL_LINK:
		  return handleLink(a0, a1);
	  case SYSCALL_SYMLINK:
		  return handleSymlink(a0, a1);
	  default:
		  return super.handleSyscall(syscall, a0, a1, a2, a3);
	}
  }

  public void handleException (int cause)
  {
    if (cause == Processor.exceptionSyscall)
    {
    	Processor processor = Machine.processor();
		int result = handleSyscall(processor.readRegister(Processor.regV0),
				processor.readRegister(Processor.regA0), processor
						.readRegister(Processor.regA1), processor
						.readRegister(Processor.regA2), processor
						.readRegister(Processor.regA3));
		processor.writeRegister(Processor.regV0, result);
		processor.advancePC();
    }
    else
      super.handleException(cause);
  }
  
  protected int handleGetcwd(int des, int size) {
	String Current_path = FilesysKernel.realFileSystem.Current_path_string();
	int leng = Current_path.length();
	if (leng >= size) return -1;
	byte[] res = new byte[leng + 1];
	System.arraycopy(Current_path.getBytes(), 0, res, 0, Current_path.length());
	res[res.length - 1] = 0;
	writeVirtualMemory(des, res);
	return leng;
  }
  
  protected int handleReaddir(int dir, int des, int count, int leng) {
	String path = readVirtualMemoryString(dir, Max_string_length);
	String[] folders = FilesysKernel.realFileSystem.readDir(path);
	if (folders == null) return -1;
	if (folders.length > count) return -1;
	for (String st : folders)
		if (st.length() >= leng) return -1;
	byte[] res = new byte[count * leng];
	for (int i = 0; i < folders.length; ++i) {
		byte[] st = folders[i].getBytes();
		System.arraycopy(st, 0, res, leng * i, st.length);
		res[leng * i + st.length] = 0;
	}
	writeVirtualMemory(des, res);
	return folders.length;
  }
  
  protected int handleStat(int file, int des) {
	String path = readVirtualMemoryString(file, Max_string_length);
	FileStat file_stat = FilesysKernel.realFileSystem.getStat(path);
	if (file_stat == null) return -1;
	writeVirtualMemory(des, file_stat.Stat_content());
	return FileStat.STAT_SIZE;
  }

  protected int handleLink(int from, int to) {
    String From_dir = readVirtualMemoryString(from, Max_string_length);
	String To_dir = readVirtualMemoryString(to, Max_string_length);
	if (FilesysKernel.realFileSystem.createLink(From_dir, To_dir))
		return 0;
	else return -1;
  }
  
  protected int handleSymlink(int from, int to) {
	String From_dir = readVirtualMemoryString(from, Max_string_length);
	String To_dir = readVirtualMemoryString(to, Max_string_length);
	if (FilesysKernel.realFileSystem.createSymlink(From_dir, To_dir))
		return 0;
	else return -1;
  }
}
