package nachos.filesys;

import java.util.HashSet;
import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
  /** the static address */
  public static int STATIC_ADDR = 0;
  
  /** size occupied in the disk (bitmap) */
  static int size = Lib.divRoundUp(Disk.NumSectors, 8);
  
  /** maintain address of all the free sectors */
  private LinkedList<Integer> free_list;
  
  private HashSet<Integer> used_list = new HashSet<Integer>();
  
  private int original_size;
  
  public FreeList (INode inode)
  {
    super(inode);
    free_list = new LinkedList<Integer>();
  }
  
  public void init ()
  {
    for (int i = 2; i < Disk.NumSectors; ++i)
    	free_list.add(i); 
    original_size = free_list.size();
    save();
  }
  
  /** allocate a new sector in the disk */
  public int allocate ()
  {
	return free_list.removeFirst();
  }
  
  /** deallocate a sector to be reused */
  public void deallocate (int sec)
  {
	free_list.add(sec);
	//if (used_list.contains(sec))  
	//	used_list.remove(sec);
  }
  
  /** save the content of freelist to the disk */
  public void save ()
  {
	int total = Disk.NumSectors;
	int buf_size = total /  32;
	if (total % 32 != 0) buf_size++;
	int buf[] = new int[buf_size];
	for (int i = 0; i < buf_size; ++i) buf[i] = 0;
	
	for (Integer free_sector : free_list)
		buf[free_sector / 32] |= 1 << (free_sector % 32);

	byte bytes[] = new byte[buf_size * 4];
	for (int i = 0; i < buf_size; ++i)
		Disk.extInt(buf[i], bytes, i * 4);
	write(0, bytes, 0, buf_size * 4);
	inode.save();
  }
  
  /** load the content of freelist from the disk */
  public void load()
  {
	int total = Disk.NumSectors;
	int buf_size = total /  32;
	if (total % 32 != 0) buf_size++;
	int buf[] = new int[buf_size];
	for (int i = 0; i < buf_size; ++i) buf[i] = 0;

	byte bytes[] = new byte[buf_size * 4];
	read(0, bytes, 0, buf_size*4);
	for (int i = 0; i < buf_size; ++i)
		buf[i] = Disk.intInt(bytes, i*4);
	
	for (int i = 0; i < Disk.NumSectors; ++i)
		if ( (buf[i / 32] & (1 << (i % 32))) != 0)
			free_list.add(i);
  }
  
  public void init_used_list() {
	for (int i = 0; i < Disk.NumSectors; ++i)
		used_list.add(i);
	for (Integer k : free_list)
		used_list.remove(k);
  }

  public boolean Check_used(int i) {
	return used_list.contains(i);
  }
  
  public int size() {
	return original_size;
  }
}
