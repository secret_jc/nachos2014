package nachos.filesys;

import java.util.HashMap;

import nachos.machine.Lib;

public class FileStat
{
  public static final int FILE_NAME_MAX_LEN = 256;
  public static final int NORMAL_FILE_TYPE = 0;
  public static final int DIR_FILE_TYPE = 1;
  public static final int LinkFileType = 2;
  public static final int STAT_SIZE = FILE_NAME_MAX_LEN + 20;
  
  public String name;
  public int size;
  public int sectors;
  public int type;
  public int inode;
  public int links;
	
  public byte[] Stat_content() {
	byte[] res = new byte[STAT_SIZE];
	int len = Math.min(FILE_NAME_MAX_LEN - 1, name.length());
	System.arraycopy(name.getBytes(), 0, res, 0, len);
	res[len] = 0;	
	int pos = FILE_NAME_MAX_LEN;
	Lib.bytesFromInt(res, pos, size);
	Lib.bytesFromInt(res, pos + 4, sectors);
	Lib.bytesFromInt(res, pos + 8, type);
	Lib.bytesFromInt(res, pos + 12, inode);
	Lib.bytesFromInt(res, pos + 16, links);
	return res;
  }
  
  public static final HashMap<Integer, Integer> Translate = new HashMap<Integer, Integer>();
  {
	Translate.put(INode.TYPE_FILE, NORMAL_FILE_TYPE);
	Translate.put(INode.TYPE_SYMLINK, LinkFileType);
	Translate.put(INode.TYPE_FOLDER, DIR_FILE_TYPE);
  }
}
