package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
  INode inode;
  
  private int pos;
  
  public File (INode inode)
  {
    this.inode = inode;
    pos = 0;
  }
  
  public int length ()
  {
    return inode.file_size;
  }
  
  public void close ()
  {
	  if (inode == null) return;
	  inode.Decrease_use_count();
	  inode = null;
  }
  
  public void seek (int pos)
  {
    this.pos = pos;
  }
  
  public int tell ()
  {
    return pos;
  }
  
  public int read (byte[] buffer, int start, int limit)
  {
    int ret = read(pos, buffer, start, limit);
    pos += ret;
    return ret;
  }
  
  public int write (byte[] buffer, int start, int limit)
  {
    int ret = write(pos, buffer, start, limit);
    pos += ret;
    return ret;
  }
  
  public int read(int pos, byte[] buffer, int start, int limit) 
  {
	Get_read_access();
	if (pos >= length()) {
		Free_read_access();
		return -1;
	}
	limit = Math.min(length() - pos, limit);
	int First_sector = pos / Disk.SectorSize;
	int First_pos = pos % Disk.SectorSize;
	int Last_sector = (pos + limit) / Disk.SectorSize;
	int total = Math.min(Disk.SectorSize - First_pos, limit);
	byte[] tmp = new byte[Disk.SectorSize];
	Machine.synchDisk().readSector(inode.getSector(First_sector), tmp, 0);
	System.arraycopy(tmp, First_pos, buffer, start, total);
	start += total;
	for (int i = First_sector + 1; i <= Last_sector; ++i) {
		Integer addr = inode.getSector(i);
		if (addr == null) {
			Free_read_access();
			return total;
		}
		Machine.synchDisk().readSector(addr, tmp, 0);
		int len = Math.min(limit - total, Disk.SectorSize);
		System.arraycopy(tmp, 0, buffer, start, len);
		total += len;
		start += len;
	}
	Free_read_access();
	return total;
  }

  public int write(int pos, byte[] buffer, int start, int limit) 
  {
	Get_write_access();
	if (pos + limit >= length())
		inode.setFileSize(pos + limit);
	int First_sector = pos / Disk.SectorSize;
	int First_pos = pos % Disk.SectorSize;
	int Last_sector = (pos + limit) / Disk.SectorSize;
	int total = Math.min(Disk.SectorSize - First_pos, limit);
	byte[] tmp = new byte[Disk.SectorSize];
	Machine.synchDisk().readSector(inode.getSector(First_sector), tmp, 0);
	System.arraycopy(buffer, start, tmp, First_pos, total);
	Machine.synchDisk().writeSector(inode.getSector(First_sector), tmp, 0);
	start += total;
	for (int i = First_sector + 1; i <= Last_sector; ++i) {
		Integer addr = inode.getSector(i);
		if (addr == null) {
			Free_write_access();
			return total;
		}
		Machine.synchDisk().readSector(addr, tmp, 0);
		int len = Math.min(limit - total, Disk.SectorSize);
		System.arraycopy(buffer, start, tmp, 0, len);
		Machine.synchDisk().writeSector(addr, tmp, 0);
		total += len;
		start += len;
	}
	Free_write_access();
	return total;
  }

  public void Get_read_access() 
  {
	  if (!inode.access.Cur_thread_read() && !inode.access.Cur_thread_write())
		  inode.access.Get_read_access();
  }

  public void Free_read_access() 
  {
	  if (inode.access.Cur_thread_read())
		  inode.access.Free_read_access();
  }

  public void Get_write_access() 
  {
	  if (!inode.access.Cur_thread_read() && !inode.access.Cur_thread_write())
		  inode.access.Get_write_access();
  }

  public void Free_write_access() 
  {
	  if (inode.access.Cur_thread_write())
		  inode.access.Free_write_access();
  }
	
  public String Read_link(int pos, int maxLength) 
  {
	  byte[] bytes = new byte[maxLength + 1];
	  int total = read(pos, bytes, 0, maxLength + 1);
	  for (int length = 0; length < total; length++) {
		  if (bytes[length] == 0) 
			  return new String(bytes, 0, length);
	  }
	  return null;
  }
	
  public boolean Write_link(int pos, String st) {
	  byte[] bytes = new byte[st.length() + 1];
	  System.arraycopy(st.getBytes(), 0, bytes, 0, st.length());
	  bytes[st.length()] = 0;
	  return write(pos, bytes, 0, bytes.length) == bytes.length;
  }
}
