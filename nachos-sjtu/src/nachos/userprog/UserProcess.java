package nachos.userprog;

import nachos.filesys.FilesysKernel;
import nachos.machine.*;
import nachos.threads.*;

import java.io.EOFException;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		PID = Number++;
		Status = -1;

		lock.acquire();
		Active_processes.put(PID, this);
		lock.release();
		Child_process = new HashSet<Integer>();
		finished = new Semaphore(0);
		
		descriptor = new Descriptor();
		descriptor.add(UserKernel.console.openForReading());
		descriptor.add(UserKernel.console.openForWriting());
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;
		Running = Running + 1;
		new UThread(this).setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}
	
	protected TranslationEntry Get_entry(int page, boolean write) {
		if (page < 0 || page >= numPages)
			return null;
		TranslationEntry ans = pageTable[page];
		if (ans == null) return null;
		if (ans.readOnly && write) return null;
		ans.used = true;
		if (write) ans.dirty = true;
		return ans;
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);
		int Start_page = Processor.pageFromAddress(vaddr);
		int Head = Processor.offsetFromAddress(vaddr);
		int End_page = Processor.pageFromAddress(vaddr+length);
		byte[] memory = Machine.processor().getMemory();
		TranslationEntry entry = Get_entry(Start_page, false);
		if (entry == null) return 0;
		int amount = Math.min(length, pageSize - Head);
		System.arraycopy(memory, Processor.makeAddress(entry.ppn, Head), data, offset, amount);
		offset = offset + amount;
		for (int i = Start_page + 1; i <= End_page; ++i) {
			entry = Get_entry(i, false);
			if (entry == null) return amount;
			int leng = Math.min(length - amount, pageSize);
			System.arraycopy(memory, Processor.makeAddress(entry.ppn, 0), data, offset, leng);
			offset = offset + leng;
			amount = amount + leng;
		}
		
		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);
		int Start_page = Processor.pageFromAddress(vaddr);
		int Head = Processor.offsetFromAddress(vaddr);
		int End_page = Processor.pageFromAddress(vaddr+length);
		byte[] memory = Machine.processor().getMemory();
		TranslationEntry entry = Get_entry(Start_page, true);
		if (entry == null) return 0;
		int amount = Math.min(length, pageSize - Head);
		System.arraycopy(data, offset, memory, Processor.makeAddress(entry.ppn, Head), amount);
		offset = offset + amount;
		for (int i = Start_page + 1; i <= End_page; ++i) {
			entry = Get_entry(i, true);
			if (entry == null) return amount;
			int leng = Math.min(length - amount, pageSize);
			System.arraycopy(data, offset, memory, Processor.makeAddress(entry.ppn, 0), leng);
			offset = offset + leng;
			amount = amount + leng;
		}
		return amount;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");
		
		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}
		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		numPages++;

		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}


	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		int[] Page_list = UserKernel.Allocate(numPages);
		if (Page_list == null) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}
		pageTable = new TranslationEntry[numPages];

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int virtual = section.getFirstVPN() + i;
				int physical = Page_list[virtual];
				pageTable[virtual] = new TranslationEntry(virtual, physical, true, section.isReadOnly(), false, false);
				section.loadPage(i, physical);
			}
		}
		
		for (int i = 0; i < stackPages + 1; ++i) {
			int tmp = numPages + i - stackPages - 1;
			pageTable[tmp] = new TranslationEntry(tmp, Page_list[tmp], true, false, false, false);
		}
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		for (int i = 0; i < numPages; ++i) 
			UserKernel.Free(pageTable[i].ppn);
		pageTable = null;
		coff.close();
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 */
	protected int handleHalt() {
		if (UserKernel.First_process != this) 
			return 0;
		Machine.halt();

		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}
	
	protected int handleExit(int s) {
		this.Status = s;
		for (int i = 2; i < Max_descriptor_number; i++) 
			handleClose(i);
		unloadSections();
		Running = Running - 1;
		Active_processes.remove(PID);
		Finished_processes.put(PID, this);
		finished.V(); 
		if (Running == 0)
			Kernel.kernel.terminate();
		UThread.finish();
		return 0;
	}
	
	protected int handleExec(int file, int argc, int argv) {
		String name = this.readVirtualMemoryString(file, Max_string_length);
		if (name == null) return -1;
		if (!name.endsWith(".coff") || argc < 0) return -1;
		String[] args = new String[argc];
		byte[] buffer = new byte[4]; 
		for (int i = 0; i < argc; i++) {
			if (readVirtualMemory(argv + i * 4, buffer) != 4) 
				return -1;
			int tmp = Lib.bytesToInt(buffer, 0);
			args[i] = readVirtualMemoryString(tmp, Max_string_length);
			if (args[i] == null)
				return -1;
		}
		UserProcess child = newUserProcess();
		Child_process.add(child.PID);
		saveState();
		if (!child.execute(name, args)) return -1;
		return child.PID;
	}
	
	protected int handleJoin(int pID, int s) {
		if (!Child_process.contains(pID)) return -1;
		Child_process.remove(pID);
		UserProcess child = Active_processes.get(pID);
		if (child == null) {
			child = Finished_processes.get(pID);
			if (child == null) return -1;
		}
		child.finished.P();  
		byte[] tmp = Lib.bytesFromInt(child.Status);
		writeVirtualMemory(s, tmp);
		if (child.Status == -1)
			return 0;
		else
			return 1;
	}
	
	
	protected int handleCreate(int name) {
		String fileName = readVirtualMemoryString(name, Max_string_length);
		if (fileName == null) return -1;
		OpenFile file = FilesysKernel.realFileSystem.open(fileName, true);
		if (file == null) return -1;
		return descriptor.add(file);
	}

	protected int handleOpen(int name) {
		String fileName = readVirtualMemoryString(name, Max_string_length);
		if (fileName == null) return -1;
		OpenFile file = FilesysKernel.realFileSystem.open(fileName, false);
		if (file == null) return -1;
		return descriptor.add(file);
	}

	protected int handleClose(int fileDescriptor) {
		return descriptor.close(fileDescriptor);
	}

	protected int handleUnlink(int name) {
		String fileName = readVirtualMemoryString(name, Max_string_length);
		if (fileName == null) return -1;
		return FilesysKernel.realFileSystem.remove(fileName) ? 0 : -1;
	}

	private int handleRead(int f_d, int addr, int size) {
		OpenFile file = descriptor.get(f_d);
		if (file == null) return -1;
		if (addr < 0 || size < 0) return -1; 
		byte[] buffer = new byte[size];
		int bytes1 = file.read(buffer, 0, size);
		if (bytes1 == -1) return -1;     
		int bytes2 = writeVirtualMemory(addr, buffer, 0, bytes1);
		return bytes2;
	}
	
	private int handleWrite(int f_d, int addr, int size) {
		OpenFile file = descriptor.get(f_d);
		if (file == null) return -1;
		if (addr < 0 || size < 0) return -1; 
		byte[] buffer = new byte[size];
		int bytes1 = readVirtualMemory(addr, buffer, 0, size);
		int bytes2 = file.write(buffer, 0, bytes1);
		return bytes2;
	}

	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3;

	protected static final int syscallCreate = 4;

	protected static final int syscallOpen = 5;

	private static final int syscallRead = 6;

	private static final int syscallWrite = 7;

	protected static final int syscallClose = 8;

	protected static final int syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			return handleExec(a0, a1, a2);
		case syscallJoin:
			return handleJoin(a0, a1);
		case syscallCreate:
			return handleCreate(a0);
		case syscallOpen:
			return handleOpen(a0);
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		case syscallUnlink:
			return handleUnlink(a0);
		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			handleExit(-1);
			Lib.assertNotReached("Unknown system call!");
		}
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			handleExit(-1);
			Lib.assertNotReached("Unexpected exception");
		}
	}
	
	public class Descriptor {

		public int empty() {
			for (int i = 0; i < Max_descriptor_number; i++)
				if (List[i] == null)
					return i;
			return -1;
		}
		
		public int add(OpenFile file) {
			int index = empty();
			if (index == -1) return -1;
			return add(index, file);
		}
		
		public int add(int x, OpenFile file) {
			if (x < 0 || x >= Max_descriptor_number)
				return -1;
			if (List[x] == null) {
				List[x] = file;
				return x;
			}
			return -1;
		}
		
		public int close(int x) {
			if (List[x] == null) {
				return -1;
			}
			OpenFile file = List[x];
			List[x] = null;
			file.close();
			return 0;
		}

		
		public OpenFile get(int index) {
			if (index >= Max_descriptor_number || index < 0)
				return null;
			return List[index];
		}
		
		public OpenFile List[] = new OpenFile[Max_descriptor_number];
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	protected int initialPC, initialSP;
	protected int argc, argv;
	
	protected static final int pageSize = Processor.pageSize;
	protected static final char dbgProcess = 'a';
	
	protected static final int Max_string_length = 256;
	protected static final int Max_descriptor_number = 16;
	
	protected int PID;
	private int Status;
	private static int Number = 0;
	protected static int Running = 0;
	private Semaphore finished; 
	protected static Lock lock = new Lock(); 
	protected Descriptor descriptor;
	private HashSet<Integer> Child_process;
	protected static Hashtable<Integer, UserProcess> Active_processes = new Hashtable<Integer, UserProcess>();
	protected static Hashtable<Integer, UserProcess> Finished_processes = new Hashtable<Integer, UserProcess>();
};
